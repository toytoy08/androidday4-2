import { createStore , combineReducers} from 'redux'
import ProfileReducer from './profileReducers'
import ProductReducer from './productReducers'

const reducer = combineReducers({
  profile: ProfileReducer,
  product: ProductReducer
})

const store = createStore(reducer)
export default store