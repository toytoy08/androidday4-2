import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import myStyle from '../style'

class Profile extends Component {

    goBack = () => {
        this.props.history.push('/main')
    }

    goEdit = () => {
        this.props.history.push('/profile/edit')
    }

    logout = () => {
        this.props.history.push('/login')
    }

    render() {
        const { profile } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Profile
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <View>
                        <Text style={myStyle.bodyText}>Username :</Text>
                        <Text style={myStyle.bodyTextContent}>{profile[profile.length - 1].username}</Text>
                    </View>

                    <View>
                        <Text style={myStyle.bodyText}>Firstname :</Text>
                        <Text style={myStyle.bodyTextContent}>{profile[profile.length - 1].firstname}</Text>
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Lastname :</Text>
                        <Text style={myStyle.bodyTextContent}>{profile[profile.length - 1].lastname}</Text>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity onPress={() => { this.goEdit() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Edit'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.profile
    }
}

export default connect(mapStateToProps)(Profile)