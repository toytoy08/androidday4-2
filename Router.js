
import React, { Component } from 'react';
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'

import Login from './Mainapp/Login'
import Profile from './Profile/Profile';
import EditProfile from './Profile/EditProfile';
import Product from './Product/Product'
import AddProduct from './Product/AddProduct'
import EditProduct from './Product/EditProduct'
import MainApp from './Mainapp/Main'

import { Provider } from 'react-redux'
import store from './Store'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <NativeRouter>
                    <Switch>
                        <Route exact path="/main" component={MainApp} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/profile" component={Profile} />
                        <Route exact path="/profile/edit" component={EditProfile} />
                        <Route exact path="/product" component={Product} />
                        <Route exact path="/product/edit" component={EditProduct} />
                        <Route exact path="/product/add" component={AddProduct} />
                        <Redirect to="/login" />
                    </Switch>
                </NativeRouter>
            </Provider>

        )
    }
}

export default Router