
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux'
import myStyle from '../style'

class Product extends Component {

    state = {
        pic: 'https://www.almau.edu.kz/img/no_image.png'
    }

    goBack = () => {
        this.props.history.push('/main')
    }

    goEdit = () => {
        this.props.history.push('/product/edit')
    }

    render() {
        const { product } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Product
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ width: 300, height: 200 }} source={{ uri: product[product.length - 1].pic }} />
                    </View>
                    <View>
                        <Text style={myStyle.bodyText}>Name</Text>
                        <Text style={myStyle.bodyTextContent}>{product[product.length - 1].name}</Text>
                    </View>
                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity style={{}} onPress={() => { this.goEdit() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Edit'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.product
    }
}

export default connect(mapStateToProps)(Product)