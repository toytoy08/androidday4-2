
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TextInput, Image } from 'react-native';
import { connect } from 'react-redux'
import myStyle from '../style'

class EditProduct extends Component {

    goBack = () => {
        this.props.history.push('/product')
    }

    render() {
        const { product } = this.props
        return (
            <View style={myStyle.container}>

                {/* Header */}
                <View style={myStyle.headerArea}>

                    <View style={myStyle.headerBackArrowBlock}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >

                            <View style={myStyle.backView}>
                                <Text style={myStyle.backText}>{'<='}</Text>
                            </View>

                        </TouchableOpacity>
                    </View>

                    <View style={myStyle.headerViewText}>
                        <Text style={myStyle.headerText}>
                            Edit Product
                        </Text>
                    </View>

                </View>
                {/* Header */}

                {/* body */}
                <View style={myStyle.body}>

                    <TouchableOpacity>
                        <View style={{ alignItems: 'center' }}>
                            <Image style={{ width: 300, height: 200 }} source={{ uri: product[product.length - 1].pic }} />
                        </View>
                    </TouchableOpacity>

                    <View>
                        <Text style={myStyle.bodyText}>Name</Text>
                    </View>

                    <View style={{ backgroundColor: 'white' }}>
                        <TextInput style={myStyle.bodyTextContent} maxLength={20} placeholder='Product name'>{product[product.length - 1].name}</TextInput>
                    </View>

                </View>
                {/* body */}

                {/* footer */}
                <View>

                    <View style={myStyle.footeView}>
                        <TouchableOpacity onPress={() => { this.goBack() }} >
                            <View style={myStyle.footerTouch}>
                                <Text style={myStyle.footerText}>{'Save'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                {/* footer */}

            </View >

        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.product
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addItem: (name, pic) => {
            dispatch({
                type: 'ADD_ITEM',
                name: name,
                pic: pic
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProduct)