import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    },
    headerArea: {
        flexDirection: 'row',
        backgroundColor: '#FFD027'
    },
    headerBackArrowBlock: {
        flex: 1
    },
    headerViewText: {
        flex: 2,
        backgroundColor: '#162C9a',
        borderWidth: 2,
        padding: 5
    },
    headerText: {
        fontSize: 40,
        textAlign: 'center',
        color: '#FFD027'
    },
    body: {
        flex: 1,
        backgroundColor: 'lightblue'
    },
    bodyText: {
        color: '#000000',
        fontSize: 40
    },
    bodyTextContent: {
        color: '#162C9a',
        fontSize: 30,
    },
    footeView: {
        padding: 2
    },
    footerTouch: {
        alignItems: 'center',
        backgroundColor: '#162C9a',
        borderWidth: 2,
        padding: 5
    },
    footerText: {
        fontSize: 40,
        textAlign: 'center',
        color: '#FFD027'
    },
    backView: {
        alignItems: 'center',
        backgroundColor: '#FFD027',
        borderWidth: 2,
        padding: 5
    },
    backText: {
        fontSize: 40,
        textAlign: 'center',
        color: '#162C9a'
    },
})